curl --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" --form "variables[][key]=APP_IMAGE" --form "variables[][value]=$REGISTRY_URL" \
     --form "variables[][key]=NGINX_IMAGE" --form "variables[][value]=$REGISTRY_URL_NGINX"\
     --form "variables[][key]=NGINX_PORT" --form "variables[][value]=8001" "https://gitlab.com/api/v4/projects/28727247/pipeline?ref=Dev" | jq .
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/28727247/jobs" | jq '.[0] |select(.name == "dev_run") | .id')
curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/28727247/jobs/$JOB_ID/play"
